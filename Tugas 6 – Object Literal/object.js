console.log("Soal no 1");

var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr) {
    var bio = {}
    if (arr.length == 0) {
        console.log('""');
    } else {
        for (let i = 0; i < arr.length; i++) {
            bio["firstName"] = arr[i][0];
            bio["lastName"] = arr[i][1];
            bio["gender"] = arr[i][2];
            bio["age"] = (thisYear - arr[i][3] >= 0) ? thisYear - arr[i][3] : "Invalid Birth Year";
            console.log(`${i + 1}. ${bio.firstName} ${bio.lastName}`);
            console.log(bio);
        }
    }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""
console.log("Soal no 2");

function shoppingTime(memberId, money) {
    var barang = [
        ["Sepatu brand Stacattu", 1500000],
        ["Baju brand Zoro", 500000],
        ["Baju brand H&N", 250000],
        ["Sweater brand Uniklooh", 175000],
        ["Casing Handphone ", 50000]
    ]
    var changeMoney = money
    var arrPurchased = []
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }
    for (var i = 0; i < barang.length; i++) {
        if (changeMoney >= barang[i][1]) {
            changeMoney -= barang[i][1]
            arrPurchased.push(barang[i][0])
        } else {
            continue
        }
    }
    var barang = {
        memberId: memberId,
        money: money,
        listPurchased: arrPurchased,
        changeMoney: changeMoney
    }
    return barang
}




// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("Soal no 3")
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arr = []
    for (let i in arrPenumpang) {
        var penumpang = {
            'penumpang': arrPenumpang[i][0],
            'naikDari': arrPenumpang[i][1],
            'tujuan': arrPenumpang[i][2],
            'bayar': (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000
        }
        arr.push(penumpang);
    }
    return arr;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]