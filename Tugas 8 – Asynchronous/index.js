// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]
// Tulis code untuk memanggil function readBooks di sini
let time = 10000;
let i = 0;
function readBooksCallback(time, books) {
    if (i < books.length) {
        readBooks(time, books[i], function (check) {
            if (check)
                readBooksCallback(time, books)
        })
        time -= books[i].timeSpent
        i++;
    }
}
readBooksCallback(time, books)