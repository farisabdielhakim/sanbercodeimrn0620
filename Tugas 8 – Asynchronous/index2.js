var readBooksPromise = require('./promise.js');
const readBooks = require('./callback.js');

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]
// Lanjutkan code untuk menjalankan function readBooksPromise 
let time = 10000;
let i = 0;
function readBook(time, books) {
    if (i < books.length) {
        readBooksPromise(time, books[i])
            .then(function (fullfiled) {
                readBook(time, books)
            })
            .catch(function (error) {
            })
        time -= books[i].timeSpent
        i++;
    }
}
readBook(time, books)