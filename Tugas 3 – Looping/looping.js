//Soal 1
console.log("Soal 1");
let i = 0;
console.log("LOOPING PERTAMA");
while (i <= 20) {
    console.log(i + " - I love coding");
    i += 2;
}
let j = 20;
console.log("LOOPING KEDUA");
while (j >= 2) {
    console.log(j + " - I will become a mobile developer");
    j -= 2;
}

//Soal 2
console.log("Soal 2");
let words = "";
for (let i = 1; i <= 20; i++) {
    if (i % 2 == 0) {
        words = "Berkualitas";
    } else if (i % 2 == 1 && i % 3 != 0) {
        words = "Santai";
    } else {
        words = "I Love Coding";
    }
    console.log(i + " - " + words);
}
//Soal 3
console.log("Soal 3");
let string = "";
for (let i = 0; i < 4; i++) {
    for (let j = 1; j < 8; j++) {
        process.stdout.write(string + "#");
    }
    console.log("#");
}


console.log("Soal 4");
//Soal 4
let string2 = "";
for (let i = 0; i < 7; i++) {
    string2 += "#";
    console.log(string2);
}
//Soal 5
console.log("Soal 5");
let string3 = " # ";
for (let i = 0; i < 8; i++) {
    for (let j = 0; j < 3; j++) {
        if (i % 2 == 1) {
            process.stdout.write("# ");
        } else {
            process.stdout.write(" #");
        }
    }
    if (i % 2 == 1) {
        console.log("# ");
    } else {
        console.log(" #");
    }
}
