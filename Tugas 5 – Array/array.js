console.log("soal 1");
function range(startNum, finishNum) {
    let number = [];
    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i++) {
            number.push(i);
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i--) {
            number.push(i);
        }
    } else {
        number = -1;
    }
    return number;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("soal 2");
function rangeWithStep(startNum, finishNum, step) {
    let number = [];
    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            number.push(i);
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i -= step) {
            number.push(i);
        }
    } else {
        number = -1;
    }
    return number;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("soal 3");
function sum(a, b, c) {
    let total = 0;
    let number = [];
    if (a && !b && !c) {
        total += a;
    } else if (a, b, c) {
        number = rangeWithStep(a, b, c);
        for (const i in number) {
            total += number[i];
        }
    } else {
        number = range(a, b);
        for (const i in number) {
            total += number[i];
        }
    }
    return total;
}
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("soal 4");
//contoh input
function dataHandling(n) {
    let name = [];
    for (const i of n) {
        name = `Nomor ID: ${i[0]} \nNama Lengkap: ${i[1]} \nTTL: ${i[2]} \nHoby:${i[3]} \n`;
        console.log(name);
    }
    return name;
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling(input);

console.log("soal 5");
function balikKata(string) {
    let word = '';
    for (let i = string.length - 1; i >= 0; i--) {
        word += string[i];
    }
    return word;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("Soal 6");
function dataHandling2(string) {
    string.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro");
    let nama = string[1].split(" ");
    let date = string[3].split("/");
    let bulan = date[0, 1];
    let dateDesc = date.sort(function (a, b) {
        return b - a;
    });
    switch (bulan.toString()) {
        case "01":
            bulan = "Januari";
            break;
        case "02":
            bulan = "Februari";
            break;
        case "03":
            bulan = "Maret";
            break;
        case "04":
            bulan = "April";
            break;
        case "05":
            bulan = "Mei";
            break;
        case "06":
            bulan = "Juni";
            break;
        case "07":
            bulan = "Juli";
            break;
        case "08":
            bulan = "Agustus";
            break;
        case "09":
            bulan = "September";
            break;
        case "10":
            bulan = "Oktober";
            break;
        case "11":
            bulan = "November";
            break;
        case "12":
            bulan = "Desember";
            break;
    }
    console.log(string);
    console.log(bulan);
    console.log(dateDesc);
    console.log(date.reverse().join("-"));
    console.log(nama.slice(0, 2).join(" "));
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */

