import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
export default class SkillItem extends React.Component {
    render() {
        let skill = this.props.skill
        return (
            <View style={styles.container}>
                <Icon name={skill.iconName} size={75} color='#003366'></Icon>
                <View style={styles.skillDesc}>
                    <Text style={styles.skillName} >{skill.skillName}</Text>
                    <Text style={styles.skillCategory}>{skill.category}</Text>
                    <Text style={styles.skillPercentage}>{skill.percentageProgress}</Text>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        height: 129,
        width: 343,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: '#0099FF',
        borderWidth: 5,
        borderRadius: 25,
        elevation: 3,
    },
    iconName: {
        textAlign: 'left'
    },
    skillDesc: {
        flex: 1,
        marginLeft: 15
    },
    skillName: {
        color: '#003366',
        fontSize: 24,
        fontWeight: '700',
        flex: 1
    },
    skillCategory: {
        color: '#3EC6FF',
        fontSize: 16,
        fontWeight: 650
    },
    skillPercentage: {
        color: '#003366',
        fontSize: 48,
        fontWeight: 'bold',
        fontFamily: 'roboto',
        textAlign: 'right'
    }

});