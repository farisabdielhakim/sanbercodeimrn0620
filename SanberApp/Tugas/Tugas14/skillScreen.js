import React from 'react'
import { Text, View, StyleSheet, ImageBackground, Image, ScrollView, FlatList } from 'react-native'
import data from './skillData.json'
import SkillItem from './components/SkillItem'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class SkillScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground style={styles.headerAbout} source={require('../../assets/headerAbout.jpg')}>
                    <Image style={styles.imageProfile} source={require('../../assets/profile.jpg')}>
                    </Image>
                    <Text style={styles.profileName} > Faris Abdi El Hakim </Text>
                </ImageBackground>
                <View style={styles.body}>
                    <View style={styles.listCategory}>
                        <Icon color='#0099FF' name='menu-left' size={70}>
                        </Icon>
                        <Text style={styles.textCategory}>All Category</Text>
                        <Icon color='#0099FF' name='menu-right' size={70}></Icon>
                    </View>
                    <ScrollView>
                        <FlatList
                            data={data.items}
                            renderItem={(skill) => <SkillItem skill={skill.item} />}
                            keyExtractor={(item) => item.id}
                            ItemSeparatorComponent={() => <View style={{ backgroundColor: '#B4E9FF', height: 0.5 }} />}
                        ></FlatList>
                    </ScrollView>
                </View>
            </View >
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    headerAbout: {
        width: 420,
        height: 350,
        paddingTop: 10,
        alignItems: 'center'
    },
    imageProfile: {
        width: 200,
        height: 198,
        borderRadius: 100,
        borderWidth: 4,
        borderColor: 'white',
        marginTop: 20
    },
    profileName: {
        fontSize: 24,
        color: 'white',
        fontWeight: '600',
        textDecorationLine: 'underline',
        marginBottom: 50,
    },
    listCategory: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textCategory: {
        fontSize: 20,
        color: '#0099FF',
        fontWeight: '700'
    },
    menuName: {
        flex: 2
    }
});