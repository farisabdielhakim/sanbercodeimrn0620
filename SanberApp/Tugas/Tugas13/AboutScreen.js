import React from 'react'
import { Text, View, StyleSheet, Image, ImageBackground } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
export default class AboutScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground style={styles.headerAbout} source={require('../../assets/headerAbout.jpg')}>
                    <Image style={styles.imageProfile} source={require('../../assets/profile.jpg')}>
                    </Image>
                    <Text style={styles.profileName} > Faris Abdi El Hakim </Text>
                </ImageBackground>
                <View style={styles.profile}>
                    <Image style={styles.profileUser} source={require('../../assets/fb.jpg')}></Image>
                    <Text style={styles.textProfile}>/farisabdil.hakim</Text>
                </View>
                <View style={styles.profile}><Image style={styles.profileUser} source={require('../../assets/ig.jpg')}></Image>
                    <Text style={styles.textProfile}>@farisabdiel16</Text>
                </View>
                <View style={styles.profile}><Image style={styles.profileUser} source={require('../../assets/twitter.jpg')}></Image>
                    <Text style={styles.textProfile}>_FEL_16</Text>
                </View >
                <View style={styles.profile}><Image style={styles.profileUser} source={require('../../assets/gitlab.jpg')}></Image>
                    <Text style={styles.textProfile}>/farisabdielhakim</Text>
                </View>

            </View >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        marginBottom: 10
    },
    headerAbout: {
        width: 420,
        height: 350,
        paddingTop: 10,
        alignItems: 'center'
    },
    imageProfile: {
        width: 200,
        height: 198,
        borderRadius: 100,
        borderWidth: 4,
        borderColor: 'white',
        marginTop: 20
    },
    profile: {
        flexDirection: 'row',
        paddingBottom: 20,
        paddingLeft: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileName: {
        fontSize: 24,
        color: 'white',
        fontWeight: '600',
        textDecorationLine: 'underline',
        marginBottom: 50
    },
    aboutTitle: {
        marginLeft: 0,
        fontSize: 22,
        fontWeight: '700',
        color: '#0099FF',
        textAlign: 'right'
    },
    profileUser: {
        width: 70,
        height: 70,
        marginRight: 20
    },
    textProfile: {
        fontSize: 22,
        color: '#040E42',
        fontWeight: '700'
    }
});