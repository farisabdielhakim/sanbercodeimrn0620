import React from 'react'
import { Image, StyleSheet, Text, View, TouchableOpacity, TextInput, Button, ImageBackground } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class LoginScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground style={styles.imageBackground} resizeMode='stretch' source={require('../../assets/headerLogin.jpg')}>
                    <View style={styles.body}>
                        <Text style={styles.appTitle}>SanberApp</Text>
                        <Text style={styles.text}>Welcome to SanberApp</Text>
                        <View style={styles.Content}>
                            <Text style={styles.text}>Username</Text>
                            <TouchableOpacity>
                                <TextInput style={styles.textInput}></TextInput>
                            </TouchableOpacity>
                            <Text style={styles.text} alignItems='right'>Password</Text>
                            <TouchableOpacity>
                                <TextInput style={styles.textInput}></TextInput>
                            </TouchableOpacity>
                            <Text style={[styles.text, styles.right]}>Forget Password?</Text>
                        </View>
                        <TouchableOpacity>
                            <View style={styles.buttonLogin}>
                                <Text style={styles.textButton}>Login</Text>
                            </View>
                        </TouchableOpacity>
                        <Text style={styles.text}>Dont have an account?
                        <Text style={{ paddingLeft: 4, textDecorationLine: 'underline', fontWeight: '700' }}>Sign Up</Text></Text>
                        <Text style={styles.text}>OR</Text>
                        <TouchableOpacity>
                            <View style={styles.buttonSignGoogle}>
                                <Image style={styles.logoGoogle} source={require('../../assets/logoGoogle.jpg')}></Image>
                                <Text style={styles.textButton}>Sign In With Google</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    imageBackground: {
        width: 420,
        height: 240
    },
    container: {
        backgroundColor: 'white'
    },
    body: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    right: {
        textAlign: 'right'
    },
    appTitle: {
        fontSize: 72,
        fontFamily: 'Galada',
        color: '#040E42',
        marginTop: 80,
        marginBottom: 4
    },
    textInput: {
        backgroundColor: 'white',
        borderColor: '#0099FF',
        borderWidth: 2,
        width: 296,
        height: 44,
        marginBottom: 10
    },
    Content: {
        marginTop: 35,
        paddingTop: 20,
        paddingBottom: 20
    },
    buttonLogin: {
        fontSize: 26,
        textAlign: 'center',
        opacity: 20,
        width: 296,
        height: 50,
        borderRadius: 100,
        backgroundColor: "#040E42",
        justifyContent: 'center',
        marginBottom: 10
    },
    textButton: {
        color: 'white',
        fontSize: 19,
        fontWeight: '500'
    },
    text: {
        marginBottom: 10,
        fontSize: 18,
        color: "#040E42",
        fontWeight: '500'
    },
    buttonSignGoogle: {
        width: 296,
        height: 50,
        backgroundColor: '#0099FF',
        alignItems: 'center',
        flexDirection: 'row'
    },
    logoGoogle: {
        width: 50,
        height: 50,
        borderRadius: 1,
        borderColor: 'white'
    }
})